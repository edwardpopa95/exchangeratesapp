package com.example.rowebapp.view.activities;

import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.Toast;

import com.example.rowebapp.R;
import com.example.rowebapp.databinding.ActivityMainBinding;
import com.example.rowebapp.utils.AppPreferences;
import com.example.rowebapp.utils.NetworkChangeReceiver;
import com.example.rowebapp.view.fragments.ExchangeRateFragment;

import java.util.Stack;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

public class MainActivity extends AppCompatActivity implements NetworkChangeReceiver.Listener {

    ActivityMainBinding binding;
    private Stack<Fragment> mFragmentStack;
    Fragment active;
    int mainContainer;
    boolean doubleBackToExitPressedOnce = false;
    private NetworkChangeReceiver myReceiver;
    View noInternetAlert, internetOkAlert;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        binding.setLifecycleOwner(this);
        mainContainer = R.id.mainContainer;
        prepareStack();
        noInternetAlert = binding.getRoot().findViewById(R.id.no_internet_alert);
        internetOkAlert = binding.getRoot().findViewById(R.id.internet_ok_alert);

        myReceiver = new NetworkChangeReceiver(this);
        IntentFilter intentFilter = new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");
        registerReceiver(myReceiver, intentFilter);


    }


    private void prepareStack() {

        mFragmentStack = new Stack<>();

        Fragment exchangeRateFragment = new ExchangeRateFragment();
        active = exchangeRateFragment;

        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction ft = manager.beginTransaction();
        ft.add(mainContainer, exchangeRateFragment);
        ft.show(exchangeRateFragment);
        ft.commit();
        mFragmentStack.push(active);
    }

    public void pushFragments(Fragment fragment) {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction ft = manager.beginTransaction();
        ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
        ft.hide(active);
        ft.add(mainContainer, fragment);
        ft.commit();
        active = fragment;
        mFragmentStack.push(active);
    }

    public void popFragments() {
        if (mFragmentStack.size() > 1) {
            Fragment fragmentToBeRemove = active;
            Fragment fragmentToBeShown = mFragmentStack.elementAt(mFragmentStack.size() - 2);
            mFragmentStack.pop();
            FragmentManager manager = getSupportFragmentManager();
            FragmentTransaction ft = manager.beginTransaction();
            ft.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right);
            ft.hide(active);
            android.os.Handler handler = new android.os.Handler(Looper.myLooper());
            handler.postDelayed(() -> {
                manager.beginTransaction().remove(fragmentToBeRemove).commit();
            }, 300);

            ft.show(fragmentToBeShown);
            ft.commit();
            active = fragmentToBeShown;
        }
    }

    @Override
    public void onBackPressed() {

        if (mFragmentStack.size() == 1) {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }
            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Apasă BACK din nou pentru a ieși", Toast.LENGTH_SHORT).show();
            new Handler(Looper.myLooper()).postDelayed(() -> doubleBackToExitPressedOnce = false, 2000);

        } else {
            popFragments();
        }
    }

    public Fragment getActive() {
        return active;
    }

    @Override
    public void onNetworkChangeReceived(boolean isConnected, boolean needsToShowAlert) {
        AppPreferences.setHasInternet(isConnected);
        if (needsToShowAlert && isConnected) {
            noInternetAlert.setVisibility(View.GONE);
            internetOkAlert.setVisibility(View.VISIBLE);
            Handler handler = new Handler(Looper.getMainLooper());
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    internetOkAlert.setVisibility(View.GONE);
                }
            }, 3000);

        } else {
            internetOkAlert.setVisibility(View.GONE);
            noInternetAlert.setVisibility(View.VISIBLE);
        }
    }
}
