package com.example.rowebapp.view.fragments;

import android.content.IntentFilter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.rowebapp.R;
import com.example.rowebapp.databinding.FragmentExchangeRateBinding;
import com.example.rowebapp.utils.NetworkChangeReceiver;
import com.example.rowebapp.viewmodel.ExchangeRateFragmentViewModel;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

public class ExchangeRateFragment extends Fragment  {
    ExchangeRateFragmentViewModel viewModel;
    FragmentExchangeRateBinding binding;


    public ExchangeRateFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_exchange_rate, container, false);
        binding.setLifecycleOwner(this);
        viewModel = new ViewModelProvider(this).get(ExchangeRateFragmentViewModel.class);
        binding.setViewModel(viewModel);
        return binding.getRoot();
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (hidden)
            viewModel.stopTaskExchangeRates();
        else
            viewModel.startTaskExchangeRates();
    }

    @Override
    public void onStart() {
        super.onStart();
        viewModel.startTaskExchangeRates();
    }

    @Override
    public void onStop() {
        super.onStop();
        viewModel.stopTaskExchangeRates();
    }


}
