package com.example.rowebapp.view.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;

import com.example.rowebapp.R;
import com.example.rowebapp.adapters.CustomSpinnerAdapter;
import com.example.rowebapp.databinding.FragmentExchangeRateBinding;
import com.example.rowebapp.databinding.FragmentSettingsBinding;
import com.example.rowebapp.utils.AppPreferences;
import com.example.rowebapp.viewmodel.ExchangeRateFragmentViewModel;
import com.example.rowebapp.viewmodel.SettingFragmentViewModel;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

public class SettingsFragment extends Fragment {
    SettingFragmentViewModel viewModel;
    FragmentSettingsBinding binding;



    public SettingsFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_settings, container, false);
        binding.setLifecycleOwner(this);
        viewModel = new ViewModelProvider(this).get(SettingFragmentViewModel.class);
        binding.setViewModel(viewModel);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel.setSpinnerCurrency(view.findViewById(R.id.spinnerCurrency));
        viewModel.setSpinnerTime(view.findViewById(R.id.spinnerTime));






    }
}
