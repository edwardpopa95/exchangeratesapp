package com.example.rowebapp.viewmodel;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.widget.Toast;

import com.example.rowebapp.RoWebApplication;
import com.example.rowebapp.adapters.ExchangeRatesListAdapter;
import com.example.rowebapp.model.ExchangeRate;
import com.example.rowebapp.model.ExchangeRatesResponse;
import com.example.rowebapp.repository.ExchangeRepository;
import com.example.rowebapp.utils.AppPreferences;
import com.example.rowebapp.view.activities.MainActivity;
import com.example.rowebapp.view.fragments.SettingsFragment;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import androidx.databinding.ObservableField;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

public class ExchangeRateFragmentViewModel extends ViewModel {

    private ObservableField<ExchangeRatesListAdapter> adapter = new ObservableField<>();
    public MediatorLiveData<List<ExchangeRate>> data = new MediatorLiveData<>();
    private ObservableField<String> baseCurrency = new ObservableField<>();
    private ObservableField<String> timeStamp = new ObservableField<>();
    private ObservableField<Boolean> isLoading = new ObservableField<>(false);
    private ExchangeRepository exchangeRepository = ExchangeRepository.getInstance();
    Handler handler = new Handler(Looper.getMainLooper());
    Runnable taskExchangeRate;
    private boolean isRunning;


    public ExchangeRateFragmentViewModel() {
        ExchangeRatesListAdapter exchangeRatesListAdapter = new ExchangeRatesListAdapter();
        adapter.set(exchangeRatesListAdapter);
        //    getExchangeRatesList();

    }

    public void onSettings(Context context) {
        ((MainActivity) context).pushFragments(new SettingsFragment());
    }

    public void startTaskExchangeRates() {
        if (isRunning != true) {
            isRunning = true;
            int nrOfSec = AppPreferences.getTimerValue();
            taskExchangeRate = new Runnable() {
                @Override
                public void run() {
                    getExchangeRatesList();
                    handler.postDelayed(this, nrOfSec * 1000);
                }
            };

            handler.post(taskExchangeRate);
        }

    }

    public void stopTaskExchangeRates() {
        if (isRunning) {
            isRunning = false;
            if (taskExchangeRate != null)
                handler.removeCallbacks(taskExchangeRate);
        }
    }


    public void getExchangeRatesList() {

        isLoading.set(true);
        baseCurrency.set(AppPreferences.getDefaultCurrency());
        data.addSource(exchangeRepository.getExchangeRates(AppPreferences.getDefaultCurrency()), new Observer<List<ExchangeRate>>() {
            @Override
            public void onChanged(List<ExchangeRate> exchangeRates) {
                if (exchangeRates != null) {
                    data.setValue(exchangeRates);
                    timeStamp.set(computeCurrentDate());
                    ExchangeRatesResponse exchangeRatesResponse = new ExchangeRatesResponse(exchangeRates, timeStamp.get(), AppPreferences.getDefaultCurrency());
                    AppPreferences.setExchangeRatesLastResponse(exchangeRatesResponse);
                } else {
                    if (AppPreferences.getHasInternet()) {
                        data.setValue(new ArrayList<>());
                        timeStamp.set("");
                        Toast.makeText(RoWebApplication.getContext(), "An error occurred , try again later", Toast.LENGTH_LONG).show();
                    } else {
                        ExchangeRatesResponse exchangeRatesResponse = AppPreferences.getExchangeRatesLastResponse();
                        if (exchangeRatesResponse != null) {
                            data.setValue(exchangeRatesResponse.listOfExchangeRates);
                            timeStamp.set(exchangeRatesResponse.timestamp);
                            baseCurrency.set(exchangeRatesResponse.baseCurrency);
                        } else {
                            data.setValue(new ArrayList<>());
                            timeStamp.set("");
                            Toast.makeText(RoWebApplication.getContext(), "An error occurred , try again later", Toast.LENGTH_LONG).show();
                        }
                    }
                }
                isLoading.set(false);

            }
        });

    }

    public String computeCurrentDate() {
        Date date = new Date();
        long timestamp = date.getTime();
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(timestamp);
        return cal.get(Calendar.HOUR_OF_DAY) + ":" + cal.get(Calendar.MINUTE) + ":" + cal.get(Calendar.SECOND) + " " + cal.get(Calendar.DAY_OF_MONTH) + "-" + cal.get(Calendar.MONTH) + "-" + cal.get(Calendar.YEAR);
    }


    public ObservableField<ExchangeRatesListAdapter> getAdapter() {
        return adapter;
    }

    public void setAdapter(ObservableField<ExchangeRatesListAdapter> adapter) {
        this.adapter = adapter;
    }

    public MediatorLiveData<List<ExchangeRate>> getData() {
        return data;
    }

    public void setData(MediatorLiveData<List<ExchangeRate>> data) {
        this.data = data;
    }

    public ObservableField<Boolean> getIsLoading() {
        return isLoading;
    }

    public void setIsLoading(ObservableField<Boolean> isLoading) {
        this.isLoading = isLoading;
    }

    public ObservableField<String> getBaseCurrency() {
        return baseCurrency;
    }

    public void setBaseCurrency(ObservableField<String> baseCurrency) {
        this.baseCurrency = baseCurrency;
    }

    public ObservableField<String> getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(ObservableField<String> timeStamp) {
        this.timeStamp = timeStamp;
    }
}
