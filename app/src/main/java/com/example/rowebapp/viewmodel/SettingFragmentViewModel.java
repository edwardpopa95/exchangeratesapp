package com.example.rowebapp.viewmodel;

import android.content.Context;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;

import com.example.rowebapp.adapters.CustomSpinnerAdapter;
import com.example.rowebapp.utils.AppPreferences;
import com.example.rowebapp.view.activities.MainActivity;

import java.util.Arrays;
import java.util.Set;

import androidx.lifecycle.ViewModel;

public class SettingFragmentViewModel extends ViewModel {


    Spinner spinnerCurrency;
    String[] spinnerCurrencyStrings={"EUR"} ;
    int currentSelectionCurrency = 0;
    Spinner spinnerTime;
    String[] spinnerTimerStrings ={"3","5","9"};
    int currentSelectionTimer = 0;
    AppPreferences appPreferences;
    public SettingFragmentViewModel() {

    }
    public void onBack(Context context) {
        ((MainActivity) context).popFragments();

    }

    public void setSpinnerCurrency(Spinner spinnerCurrencyToSet) {
        spinnerCurrency = spinnerCurrencyToSet;
        Set<String> currenciesSet=AppPreferences.getAllCurrencies();
        if(currenciesSet!=null)
        {
            String[] currenciesArray=new String[currenciesSet.size()];
            currenciesSet.toArray(currenciesArray);
            Arrays.sort(currenciesArray);
            spinnerCurrencyStrings=currenciesArray;
        }
        CustomSpinnerAdapter adapter = new CustomSpinnerAdapter(spinnerCurrencyStrings);
        spinnerCurrency.setAdapter(adapter);
        spinnerCurrency.setSelection(adapter.getPositionOfItem(String.valueOf(AppPreferences.getDefaultCurrency())));
        spinnerCurrency.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (currentSelectionCurrency != i) {
                    spinnerCurrency.setSelection(i);
                    currentSelectionCurrency = i;
                    appPreferences.setDefaultCurrency(spinnerCurrencyStrings[i]);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                return;
            }
        });
    }

    public void setSpinnerTime(Spinner timerSpinnerToSet) {
        spinnerTime = timerSpinnerToSet;
        CustomSpinnerAdapter adapter = new CustomSpinnerAdapter(spinnerTimerStrings);
        spinnerTime.setAdapter(adapter);
        spinnerTime.setSelection(adapter.getPositionOfItem(String.valueOf(AppPreferences.getTimerValue())));
        spinnerTime.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (currentSelectionCurrency != i) {
                    spinnerTime.setSelection(i);
                    currentSelectionTimer = i;
                    appPreferences.setTimerValue(Integer.valueOf(spinnerTimerStrings[i]));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                return;
            }
        });

    }
}
