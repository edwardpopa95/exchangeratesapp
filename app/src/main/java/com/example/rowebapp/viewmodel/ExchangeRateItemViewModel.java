package com.example.rowebapp.viewmodel;

import com.example.rowebapp.model.ExchangeRate;

import androidx.lifecycle.ViewModel;

public class ExchangeRateItemViewModel extends ViewModel {

    ExchangeRate exchangeRate;

    public ExchangeRateItemViewModel(ExchangeRate exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public ExchangeRate getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(ExchangeRate exchangeRate) {
        this.exchangeRate = exchangeRate;
    }
}
