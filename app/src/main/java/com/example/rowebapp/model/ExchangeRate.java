package com.example.rowebapp.model;

public class ExchangeRate {

    String currency;
    String baseCurrency;
    double value;


    public ExchangeRate(String currency, String baseCurrency, double value) {
        this.currency = currency;
        this.baseCurrency = baseCurrency;
        this.value = value;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getBaseCurrency() {
        return baseCurrency;
    }

    public void setBaseCurrency(String baseCurrency) {
        this.baseCurrency = baseCurrency;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }


}
