package com.example.rowebapp.model;

import java.util.List;

public class ExchangeRatesResponse {

    //data model for storing exchange rate response ( show it in offline mode)
    public List<ExchangeRate> listOfExchangeRates;
    public String timestamp;
    public String baseCurrency;



    public ExchangeRatesResponse(List<ExchangeRate> listOfExchangeRates, String timestamp, String baseCurrency) {
        this.listOfExchangeRates = listOfExchangeRates;
        this.timestamp = timestamp;
        this.baseCurrency = baseCurrency;
    }

    public List<ExchangeRate> getListOfExchangeRates() {
        return listOfExchangeRates;
    }

    public void setListOfExchangeRates(List<ExchangeRate> listOfExchangeRates) {
        this.listOfExchangeRates = listOfExchangeRates;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getBaseCurrency() {
        return baseCurrency;
    }

    public void setBaseCurrency(String baseCurrency) {
        this.baseCurrency = baseCurrency;
    }
}
