package com.example.rowebapp.repository;

import android.util.ArraySet;

import com.example.rowebapp.BuildConfig;
import com.example.rowebapp.model.ExchangeRate;
import com.example.rowebapp.network.RestAPIClient;
import com.example.rowebapp.network.RestAPIInterface;
import com.example.rowebapp.utils.AppPreferences;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ExchangeRepository {
    private static ExchangeRepository INSTANCE;
    RestAPIInterface apiInterface;

    public static ExchangeRepository getInstance() {
        if (INSTANCE == null)
            INSTANCE = new ExchangeRepository();

        return INSTANCE;
    }

    ExchangeRepository() {

        apiInterface = RestAPIClient.getClient("http://api.exchangeratesapi.io/v1/").create(RestAPIInterface.class);
    }

    public LiveData<List<ExchangeRate>> getExchangeRates(String baseCurrecy) {

        MutableLiveData<List<ExchangeRate>> exchangeRatesLiveData = new MutableLiveData<>();
        Call serverCall = apiInterface.getExchangeRates(BuildConfig.ACCESS_KEY, baseCurrecy);
        serverCall.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                if (response.isSuccessful()) {
                    List<ExchangeRate> exchangeRateList = new ArrayList<>();
                    JSONObject json;
                    try {
                        json = new JSONObject(new Gson().toJson(response.body()));
                        JSONObject jsonObject = json.getJSONObject("rates");
                        Iterator<String> ratesIterator = jsonObject.keys();
                        Set<String> setOfCurrencies = new ArraySet<>();
                        while (ratesIterator.hasNext()) {
                            String currency = ratesIterator.next();
                            setOfCurrencies.add(currency);
                            double value = jsonObject.getDouble(currency);
                            ExchangeRate exchangeRate = new ExchangeRate(currency, json.optString("base"), value);
                            exchangeRateList.add(exchangeRate);
                        }
                        AppPreferences.setAllCurrencies(setOfCurrencies);
                        exchangeRatesLiveData.setValue(exchangeRateList);


                    } catch (JSONException e) {
                        exchangeRatesLiveData.setValue(null);
                        e.printStackTrace();
                    }

                } else {

                    exchangeRatesLiveData.setValue(null);
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                exchangeRatesLiveData.setValue(null);
            }
        });


        return exchangeRatesLiveData;
    }

}
