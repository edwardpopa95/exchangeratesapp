package com.example.rowebapp.adapters;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

public abstract class CustomAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>  {

    public abstract void clearData();
    public abstract void updateData(List list);

}
