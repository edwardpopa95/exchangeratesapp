package com.example.rowebapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.rowebapp.R;
import com.example.rowebapp.RoWebApplication;

public class CustomSpinnerAdapter extends BaseAdapter {

    Context context;
    String[] items;
    LayoutInflater inflater;

    public CustomSpinnerAdapter(String[] spinnerItems) {
        this.context = RoWebApplication.getContext();
        this.items = spinnerItems;
        inflater = (LayoutInflater.from(this.context));


    }

    @Override
    public int getCount() {
        return items.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public int getPositionOfItem(String item) {
        for (int i = 0; i < items.length; i++) {
            if (item.equalsIgnoreCase(items[i]))
                return i;
        }
        return -1;
    }

    @Override
    public View getView(int i, View view, ViewGroup parent) {
        view = inflater.inflate(R.layout.custom_spinner_item, null);
        TextView itemNameTextView = view.findViewById(R.id.item_text);
        itemNameTextView.setText(items[i]);
        return view;
    }
}
