package com.example.rowebapp.adapters.viewholder;

import android.view.View;

import com.example.rowebapp.databinding.ExchangeRateItemCardBinding;
import com.example.rowebapp.viewmodel.ExchangeRateItemViewModel;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

public class ExchangeRateItemViewHolder  extends RecyclerView.ViewHolder {
    ExchangeRateItemCardBinding binding;
    public ExchangeRateItemViewHolder(@NonNull View itemView) {
        super(itemView);
        bind();

    }

    public void bind() {
        if (binding == null) {
            binding = DataBindingUtil.bind(itemView);
        }
    }

    public void unbind() {
        if (binding != null) {
            binding.unbind();
        }
    }

    public void setViewModel(ExchangeRateItemViewModel viewModel) {
        if (binding != null) {
            binding.setViewModel(viewModel);
        }
    }
}
