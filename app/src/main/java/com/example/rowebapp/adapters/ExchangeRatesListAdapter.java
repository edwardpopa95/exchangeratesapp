package com.example.rowebapp.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.rowebapp.R;
import com.example.rowebapp.adapters.viewholder.ExchangeRateItemViewHolder;
import com.example.rowebapp.model.ExchangeRate;
import com.example.rowebapp.viewmodel.ExchangeRateItemViewModel;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ExchangeRatesListAdapter extends CustomAdapter {

    private List<ExchangeRate> data;

    public ExchangeRatesListAdapter() {
        data = new ArrayList<>();
    }

    @Override
    public void clearData() {
        if (data != null && !data.isEmpty()) {
            data.clear();
            notifyDataSetChanged();
        }
    }

    @Override
    public void updateData(List list) {
        if (list != null && !list.isEmpty()) {
            int startPosition = 0;
            if (data.isEmpty()) {
                this.data.addAll(list);
                notifyItemRangeInserted(startPosition, list.size());
            } else {
                this.data.clear();
                this.data.addAll(list);
                notifyItemRangeChanged(startPosition, list.size());
            }

        }else
        {
            this.data.clear();
            notifyDataSetChanged();
        }

    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        View itemView;

        itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.exchange_rate_item_card, parent, false);
        viewHolder = new ExchangeRateItemViewHolder(itemView);


        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ExchangeRate exchangeRate = data.get(position);
        ((ExchangeRateItemViewHolder) holder).setViewModel(new ExchangeRateItemViewModel(exchangeRate));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}
