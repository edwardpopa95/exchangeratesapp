package com.example.rowebapp.utils;

import com.example.rowebapp.adapters.CustomAdapter;

import java.util.List;

import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class RecycleViewBindingAdapter {

    //binding adapter for setting up the recycle view
    @BindingAdapter(value = {"adapter", "data"}, requireAll = false)
    public static void bind(RecyclerView recyclerView, CustomAdapter adapter, List data) {
        if (recyclerView.getAdapter() == null) {
            recyclerView.setAdapter(adapter);
            recyclerView.setHasFixedSize(true);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(recyclerView.getContext(), LinearLayoutManager.VERTICAL, false);
            recyclerView.setLayoutManager(linearLayoutManager);
        }
        if (adapter != null && data != null)
            adapter.updateData(data);


    }

}
