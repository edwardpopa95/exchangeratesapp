package com.example.rowebapp.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.rowebapp.RoWebApplication;
import com.example.rowebapp.model.ExchangeRatesResponse;
import com.google.gson.Gson;

import java.util.Set;

public class AppPreferences {



    private static final String SHARED_PREFERENCES_NAME = "roweb_test_app";
    private static SharedPreferences mSharedPreferences;

    public static void setExchangeRatesLastResponse(ExchangeRatesResponse exchangeRatesLastResponse) {
        mSharedPreferences = RoWebApplication.getContext().getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = mSharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(exchangeRatesLastResponse);
        prefsEditor.putString("exchange_rates_last_response", json).commit();
        prefsEditor.commit();

    }

    public static ExchangeRatesResponse getExchangeRatesLastResponse() {
        mSharedPreferences = RoWebApplication.getContext().getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = mSharedPreferences.getString("exchange_rates_last_response", "");
        return gson.fromJson(json, ExchangeRatesResponse.class);
    }


    public static void setAllCurrencies(Set<String> allCurrencies) {
        mSharedPreferences = RoWebApplication.getContext().getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = mSharedPreferences.edit();
        prefsEditor.putStringSet("all_currencies", allCurrencies).commit();
        prefsEditor.commit();
    }

    public static Set<String> getAllCurrencies() {
        mSharedPreferences = RoWebApplication.getContext().getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        return mSharedPreferences.getStringSet("all_currencies", null);
    }

    public static void setDefaultCurrency(String defaultCurrency) {
        mSharedPreferences = RoWebApplication.getContext().getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = mSharedPreferences.edit();
        prefsEditor.putString("default_currency", defaultCurrency).commit();
        prefsEditor.commit();
    }

    public static String getDefaultCurrency() {
        mSharedPreferences = RoWebApplication.getContext().getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        return mSharedPreferences.getString("default_currency", "EUR");
    }

    public static void setTimerValue(int timeValue) {
        mSharedPreferences = RoWebApplication.getContext().getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = mSharedPreferences.edit();
        prefsEditor.putInt("timer_value", timeValue).commit();
        prefsEditor.commit();
    }

    public static int getTimerValue() {
        mSharedPreferences = RoWebApplication.getContext().getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        return mSharedPreferences.getInt("timer_value", 3);
    }

    public static void setHasInternet(boolean hasInternet) {
        mSharedPreferences = RoWebApplication.getContext().getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = mSharedPreferences.edit();
        prefsEditor.putBoolean("has_internet", hasInternet).commit();
        prefsEditor.commit();
    }

    public static boolean getHasInternet() {
        mSharedPreferences = RoWebApplication.getContext().getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        return mSharedPreferences.getBoolean("has_internet", true);
    }
}
