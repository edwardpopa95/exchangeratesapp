package com.example.rowebapp.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetworkChangeReceiver extends BroadcastReceiver {

    private static boolean comingFromNoConnection = false;

    private Listener listener;

    public interface Listener {

        void onNetworkChangeReceived(boolean isConnected, boolean needsToShowAlert);

    }

    public NetworkChangeReceiver(Listener listener) {
        this.listener = listener;
    }

    @Override
    public void onReceive(final Context context, final Intent intent) {
        if (listener == null || intent.getExtras() == null)
            return;

        final ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo ni = connectivityManager.getActiveNetworkInfo();

        if (ni != null && ni.isConnectedOrConnecting() && comingFromNoConnection) {
            listener.onNetworkChangeReceived(true, comingFromNoConnection);
            comingFromNoConnection = false;
        } else if (intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, false)) {
            comingFromNoConnection = true;
            listener.onNetworkChangeReceived(false, comingFromNoConnection);
        }
    }

}