package com.example.rowebapp.network;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RestAPIInterface {

    //API CALLS

    @GET("latest")
    Call<Object> getExchangeRates(@Query("access_key") String accessKey, @Query("base") String baseCurrency);
}
